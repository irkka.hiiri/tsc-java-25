package ru.tsc.ichaplygina.taskmanager.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.ichaplygina.taskmanager.api.IRepository;
import ru.tsc.ichaplygina.taskmanager.model.AbstractModel;

import java.util.*;

public abstract class AbstractModelRepository<E extends AbstractModel> implements IRepository<E> {

    @NotNull
    protected final Map<String, E> map = new LinkedHashMap<>();

    @Override
    public void add(@NotNull final E entity) {
        map.put(entity.getId(), entity);
    }

    @NotNull
    @Override
    public List<E> findAll() {
        return new ArrayList<>(map.values());
    }

    @Nullable
    @Override
    public E findById(@NotNull final String id) {
        return map.get(id);
    }

    @Override
    public int getSize() {
        return map.size();
    }

    @Override
    public boolean isEmpty() {
        return map.isEmpty();
    }

    @Override
    public void remove(final @NotNull E entity) {
        map.remove(entity.getId());
    }

    @Nullable
    @Override
    public E removeById(@NotNull final String id) {
        @NotNull final Optional<E> entity = Optional.ofNullable(findById(id));
        entity.ifPresent(this::remove);
        return entity.orElse(null);
    }

}
