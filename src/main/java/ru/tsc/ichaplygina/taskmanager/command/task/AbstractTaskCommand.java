package ru.tsc.ichaplygina.taskmanager.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.ichaplygina.taskmanager.api.service.IProjectTaskService;
import ru.tsc.ichaplygina.taskmanager.api.service.ITaskService;
import ru.tsc.ichaplygina.taskmanager.command.AbstractCommand;
import ru.tsc.ichaplygina.taskmanager.exception.entity.TaskNotFoundException;
import ru.tsc.ichaplygina.taskmanager.exception.other.ServiceLocatorNotFoundException;
import ru.tsc.ichaplygina.taskmanager.model.Task;

import java.util.Optional;

import static ru.tsc.ichaplygina.taskmanager.util.TerminalUtil.printLinesWithEmptyLine;

public abstract class AbstractTaskCommand extends AbstractCommand {

    @NotNull
    public static final String TASK_ID_INPUT = "Please enter task id: ";

    @NotNull
    public static final String PROJECT_ID_INPUT = "Please enter project id: ";

    {
        setNeedAuthorization(true);
    }

    @Nullable
    public String getArgument() {
        return null;
    }

    @NotNull
    protected ITaskService getTaskService() {
        if (!Optional.ofNullable(serviceLocator).isPresent()) throw new ServiceLocatorNotFoundException();
        return serviceLocator.getTaskService();
    }

    @NotNull
    protected IProjectTaskService getProjectTaskService() {
        if (!Optional.ofNullable(serviceLocator).isPresent()) throw new ServiceLocatorNotFoundException();
        return serviceLocator.getProjectTaskService();
    }

    protected void showTask(@Nullable final Task task) {
        printLinesWithEmptyLine(Optional.ofNullable(task).orElseThrow(TaskNotFoundException::new));
    }

    protected void throwExceptionIfNull(@Nullable final Task task) {
        Optional.ofNullable(task).orElseThrow(TaskNotFoundException::new);
    }

}
