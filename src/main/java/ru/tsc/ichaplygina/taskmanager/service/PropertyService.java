package ru.tsc.ichaplygina.taskmanager.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.ichaplygina.taskmanager.api.property.IApplicationProperty;
import ru.tsc.ichaplygina.taskmanager.api.property.IPasswordProperty;
import ru.tsc.ichaplygina.taskmanager.api.service.IPropertyService;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public final class PropertyService implements IPropertyService, IPasswordProperty, IApplicationProperty {

    @NotNull
    private static final String FILE_NAME = "application.properties";

    @NotNull
    private static final String PASSWORD_SECRET_PROPERTY = "password.secret";

    @NotNull
    private static final String PASSWORD_SECRET_DEFAULT_VALUE = "secret";

    @NotNull
    private static final String PASSWORD_ITERATION_PROPERTY = "password.iteration";

    private static final int PASSWORD_ITERATION_DEFAULT_VALUE = 666;

    @NotNull
    private static final String APP_VERSION_PROPERTY = "application.version";

    @NotNull
    private static final String APP_VERSION_DEFAULT_VALUE = "0.1.0";

    @NotNull
    private final Properties properties = new Properties();

    public PropertyService() {
        try {
            loadPropertiesFromFile();
        } catch (@NotNull final IOException e) {
            loadDefaults();
        }
        if (properties.isEmpty()) loadDefaults();
    }

    @Override
    public final void loadDefaults() {
        properties.setProperty(PASSWORD_SECRET_PROPERTY, PASSWORD_SECRET_DEFAULT_VALUE);
        properties.setProperty(PASSWORD_ITERATION_PROPERTY, Integer.toString(PASSWORD_ITERATION_DEFAULT_VALUE));
        properties.setProperty(APP_VERSION_PROPERTY, APP_VERSION_DEFAULT_VALUE);
    }

    @Override
    public final void loadPropertiesFromFile() throws IOException {
        @Nullable final InputStream inputStream = ClassLoader.getSystemResourceAsStream(FILE_NAME);
        if (inputStream == null) return;
        properties.load(inputStream);
        inputStream.close();
    }

    @NotNull
    @Override
    public String getApplicationVersion() {
        if (properties.containsKey(APP_VERSION_PROPERTY)) return properties.getProperty(APP_VERSION_PROPERTY);
        if (System.getenv().containsKey(APP_VERSION_PROPERTY)) return System.getenv().get(APP_VERSION_PROPERTY);
        return APP_VERSION_DEFAULT_VALUE;
    }

    @Override
    public @NotNull Integer getPasswordIteration() {
        if (!properties.containsKey(PASSWORD_ITERATION_PROPERTY)) return PASSWORD_ITERATION_DEFAULT_VALUE;
        try {
            return Integer.parseInt(properties.getProperty(PASSWORD_ITERATION_PROPERTY));
        } catch (@NotNull final NumberFormatException e) {
            return PASSWORD_ITERATION_DEFAULT_VALUE;
        }
    }

    @NotNull
    @Override
    public String getPasswordSecret() {
        return properties.getProperty(PASSWORD_SECRET_PROPERTY, PASSWORD_SECRET_DEFAULT_VALUE);
    }

}
