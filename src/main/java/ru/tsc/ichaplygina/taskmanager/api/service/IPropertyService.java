package ru.tsc.ichaplygina.taskmanager.api.service;

import ru.tsc.ichaplygina.taskmanager.api.property.IApplicationProperty;
import ru.tsc.ichaplygina.taskmanager.api.property.IPasswordProperty;

import java.io.IOException;

public interface IPropertyService extends IPasswordProperty, IApplicationProperty {

    void loadDefaults();

    void loadPropertiesFromFile() throws IOException;

}
